//
//  CameraViewController.swift
//  Pokefy
//
//  Created by Saad on 8/8/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import AVFoundation
import QuartzCore
import CoreImage
import CoreMedia

class CameraViewController: UIViewController, UIImagePickerControllerDelegate {

    @IBOutlet var imagePreviewView: UIView!
    @IBOutlet var captureImage: UIImageView!
    @IBOutlet var gridImage: UIImageView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var flashButton: UIButton!
    @IBOutlet var gridButton: UIButton!
    @IBOutlet var switchCameraButton: UIButton!
    @IBOutlet var snapPicButton: UIButton!
    
    /* Variables */
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var stillImageOutput: AVCaptureStillImageOutput?
    var frontCameraDev: AVCaptureDevice?
    var backCameraDev: AVCaptureDevice?
    var videoInputDevice: AVCaptureDeviceInput?
    
    /* Camera Booleans */
    var FrontCamera = false
    var FlashON = false
    var gridON = false
    var exposureON = false
    
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Resize Views accordingly to device screen size
        //CGRectMa
//        captureImage.frame = CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.width)
//        imagePreviewView.frame = CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.width)
//        gridImage.frame = CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.width)

        // Set up some Views & Buttons
        FrontCamera = false;
        captureImage.hidden = false;
        gridImage.hidden = true
        
        
        captureSession.stopRunning()
        
        // Check multiple cameras (Front and Back)
        if AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).count > 1  {
            // Remove all inputs and Outputs from the CaptureSession
            captureSession.beginConfiguration()
            captureSession.removeInput(videoInputDevice)
            captureSession.removeOutput(stillImageOutput)
            previewLayer?.removeFromSuperlayer()
            captureSession.commitConfiguration()
            
            // Recall the camera initialization
            dispatch_after(1, dispatch_get_main_queue(), {
                self.initializeCamera()
            })
        }
        
        
        
        //initializeCamera()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeCamera() {
        
        // Remove all inputs and Outputs from the CaptureSession
        captureSession.beginConfiguration()
        captureSession.removeInput(videoInputDevice)
        captureSession.removeOutput(stillImageOutput)
        previewLayer?.removeFromSuperlayer()
        captureSession.commitConfiguration()
        captureSession.stopRunning()
        
        // Init AVCaptureSession
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
        // Alloc and set the PreviewLayer for the camera
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        imagePreviewView.layer.addSublayer(previewLayer!)
        previewLayer?.frame = imagePreviewView.bounds
        
        // Get the 2 camera devices (Back & Front)
        let devices = AVCaptureDevice.devices()
        
        // Prepare the Back or Front Camera Devices
        //let device: AVCaptureDevice?
        for device in devices {
            if device.hasMediaType(AVMediaTypeVideo) {
                if device.position == AVCaptureDevicePosition.Back {
                    backCameraDev = device as? AVCaptureDevice
                } else {
                    frontCameraDev = device as? AVCaptureDevice
                }
            }
        }
        
        // BACK CAMERA Input ---------------------------------------------------
        if !FrontCamera {
            // Check if Back Camera has Flas
            if (backCameraDev?.isFlashModeSupported(AVCaptureFlashMode.On) != nil) {
                print("flashMode: \(backCameraDev!.flashMode.hashValue)")
                
                if backCameraDev?.flashAvailable == true {
                    flashButton.enabled = true
                    do { try backCameraDev?.lockForConfiguration()
                    } catch _ {}
                    
                    if FlashON {
                        backCameraDev?.flashMode = AVCaptureFlashMode.On
                    } else {
                        backCameraDev?.flashMode = AVCaptureFlashMode.Off
                        backCameraDev?.unlockForConfiguration()
                    }
                    
                } else {  flashButton.enabled = false  }
                
                
                // Set Default Exposure
                do { try backCameraDev?.lockForConfiguration() }
                catch _ {}
                
                backCameraDev?.setExposureTargetBias(0.0, completionHandler: nil)
                backCameraDev?.unlockForConfiguration()
                
                do {  videoInputDevice = try AVCaptureDeviceInput(device: backCameraDev)
                } catch { videoInputDevice = nil }
                
                captureSession.addInput(videoInputDevice)
            }
        } // END BACK CAMERA
        
        // FRONT CAMERA Input -----------------------------------------
        if FrontCamera {
            flashButton.enabled = false
            
            do { try frontCameraDev?.lockForConfiguration()
            } catch _ {}
            frontCameraDev?.setExposureTargetBias(0.0, completionHandler: nil)
            frontCameraDev?.unlockForConfiguration()
            
            do { videoInputDevice = try AVCaptureDeviceInput(device: frontCameraDev)
            } catch { videoInputDevice = nil }
            captureSession.addInput(videoInputDevice)
            
        } // END FRONT CAMERA
    
        // Process the StillImageOutput
        if stillImageOutput != nil {
            stillImageOutput = nil
        }
        stillImageOutput = AVCaptureStillImageOutput()
        let outputSettings: NSDictionary = NSDictionary(object: AVVideoCodecJPEG, forKey: AVVideoCodecKey)
        stillImageOutput?.outputSettings = outputSettings as [NSObject : AnyObject]
        captureSession.addOutput(stillImageOutput)
        
        captureSession.startRunning()
    }
    
    @IBAction func flashButtonTapped(sender: UIButton) {
        
        FlashON = !FlashON
        print("FlashON: \(FlashON)")
        
        // BACK Camera with Flash available
        if !FrontCamera {
            // In case the Back camera has Flash
            if backCameraDev?.isFlashModeSupported(AVCaptureFlashMode.On) != nil {
                do { try backCameraDev?.lockForConfiguration()
                } catch _ {}
                
                if FlashON { // Flash ON
                    backCameraDev?.flashMode = AVCaptureFlashMode.On
                    flashButton.setBackgroundImage(UIImage(named: "flash.png"), forState: UIControlState.Normal)
                } else {  // Flash OFF
                    backCameraDev?.flashMode = AVCaptureFlashMode.Off
                    backCameraDev?.unlockForConfiguration()
                    flashButton.setBackgroundImage(UIImage(named: "flashoff.png"), forState: UIControlState.Normal)
                }
                
            }
        }
    }
    @IBAction func snapPictureButtonTapped(sender: UIButton) {
        
        captureImage.image = nil       //Remove old image from View
        captureImage.hidden = false    //Show the captured image view
        imagePreviewView.hidden = true //Hide the live video feed
        
        // Call the method to capture the photo
        capturePhoto()
    }
    
    func capturePhoto()  {
        if stillImageOutput != nil {
            let layer : AVCaptureVideoPreviewLayer = previewLayer! as AVCaptureVideoPreviewLayer
            stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo).videoOrientation = layer.connection.videoOrientation
            
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo), completionHandler: { (imageDataSampleBuffer,error) -> Void in
                
                if ((imageDataSampleBuffer) != nil) {
                    let imageData : NSData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                    
                    // Show taken photo
                    self.captureImage.image = UIImage(data: imageData)!
                    
                    // Flip the taken photo horizontally if it has been taken from Front Camera
                    if self.FrontCamera {
                        self.captureImage.image = UIImage(CGImage: (self.captureImage.image?.CGImage)!, scale: 1.0, orientation: UIImageOrientation.LeftMirrored)
                    }
                    
                    // Stop capture Session
                    self.captureSession.stopRunning()
                    
                    self.cropImage()
                }
                
            })
        }
        flashButton.enabled = false
    }
    
    func cropImage() {
    
//        let rect:CGRect = imagePreviewView.frame
//        UIGraphicsBeginImageContextWithOptions(imagePreviewView.frame.size, true, 0.0)
//        captureImage.layer.renderInContext(UIGraphicsGetCurrentContext()!)
//        takenImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
        
        // Get taken image abnd pass it to the ImageEditor
//        captureImage.image = takenImage
        
        takenImage = cropToBounds(captureImage.image!, width: imagePreviewView.frame.size.width, height: imagePreviewView.frame.size.height)
        
        //takenImage = captureImage.image
        goToCropImageVC()
        
    }
    
    func cropToBounds(image: UIImage, width: CGFloat, height: CGFloat) -> UIImage {
        
        let contextImage: UIImage = UIImage(CGImage: image.CGImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRectMake(posX, posY, cgwidth, cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImageRef = CGImageCreateWithImageInRect(contextImage.CGImage, rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(CGImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    
    // MARK: - OPEN IMAGE EDITOR VC
    func goToCropImageVC() {
        
        // Save original taken picture into Photo Library
        if saveImageToLibrary {  UIImageWriteToSavedPhotosAlbum(takenImage!, nil, nil, nil)  }
        
        let ciVC = self.storyboard?.instantiateViewControllerWithIdentifier("CropViewController") as! CropViewController
        presentViewController(ciVC, animated: true, completion: nil)
    }
    
    @IBAction func switchCameraButtonTapped(sender: UIButton) {
        
        captureSession.stopRunning()
        FrontCamera = !FrontCamera
        // println("frontCam: \(FrontCamera)")
        
        // Check multiple cameras (Front and Back)
        if AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).count > 1  {
            // Remove all inputs and Outputs from the CaptureSession
            captureSession.beginConfiguration()
            captureSession.removeInput(videoInputDevice)
            captureSession.removeOutput(stillImageOutput)
            previewLayer?.removeFromSuperlayer()
            captureSession.commitConfiguration()
            
            // Recall the camera initialization
            dispatch_after(1, dispatch_get_main_queue(), {
                self.initializeCamera()
            })
        }
        
        if FrontCamera {
            switchCameraButton.setBackgroundImage(UIImage(named: "camerafront.png"), forState: UIControlState.Normal)
        } else {
            switchCameraButton.setBackgroundImage(UIImage(named: "cameraback.png"), forState: UIControlState.Normal)
        }
    }

    @IBAction func backButtonTapped(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func gridButtonTapped(sender: UIButton) {
        
        gridON = !gridON
        
        if gridON {
            gridImage.hidden = false
            gridButton.setBackgroundImage(UIImage(named: "grid.png"), forState: UIControlState.Normal)
        } else {
            gridImage.hidden = true
            gridButton.setBackgroundImage(UIImage(named: "gridoff.png"), forState: UIControlState.Normal)
        }
    }
}
