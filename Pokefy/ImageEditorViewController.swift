//
//  ImageEditorViewController.swift
//  Pokefy
//
//  Created by Saad on 8/16/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ImageEditorViewController: UIViewController, GADInterstitialDelegate {

    @IBOutlet var containerHolderView: UIView!
    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var admobBanner: GADBannerView!
    @IBOutlet var originalImage: UIImageView!
    @IBOutlet var containerView: UIView!
    
    @IBOutlet var imageForFilters: UIImageView!
    @IBOutlet var filtersScrollView: UIScrollView!
    @IBOutlet var filtersToolView: UIView!
    
    @IBOutlet var watermarkImg: UIImageView!
    @IBOutlet var gadBannerHeight: NSLayoutConstraint!
    var sticker: Sticker?
    var screenWidth, screenHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // interstitialAd = createAndLoadInterstitial()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        init_ad()
        watermarkImg.hidden = true
        
        
        // Get the cropped image previously made
        originalImage.image = croppedImage
        
        // Set the image for filters as the Original image
        imageForFilters.image = originalImage.image
        
        // Place Tool Views outside the screen, on the bottom
        
        // adding the blur effect
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        
        blurView.frame = filtersToolView.bounds
        filtersToolView.insertSubview(blurView, atIndex: 0)
        filtersToolView.hidden = true
        
        setupFiltersTool()
        
        setUpPreviousStickers()
        
        if let _ = sticker
        {
            setUpStickerTools()
        }
        screenWidth = UIScreen.mainScreen().bounds.size.width
        screenHeight = UIScreen.mainScreen().bounds.size.height
        
    }
    
    override func viewDidAppear(animated: Bool) {
        let hasPurchased = NSUserDefaults.standardUserDefaults().boolForKey("RemoveWatermarkAd")
        
        if !hasPurchased {
            if stickersImageArray.count == 0 {
                showInterstitialAd()
            }
        }
        
        
    }

    func init_ad() {
        let hasPurchased = NSUserDefaults.standardUserDefaults().boolForKey("RemoveWatermarkAd")
        
        if hasPurchased {
            admobBanner.hidden = true
            bottomLayoutConstraint.constant = 0.0
            
        }
        else
        {
            
            admobBanner.hidden = false
            if (UI_USER_INTERFACE_IDIOM() == .Pad) {
                bottomLayoutConstraint.constant = 90.0
                gadBannerHeight.constant = 90.0
            }
            else
            {
                bottomLayoutConstraint.constant = 50.0
                gadBannerHeight.constant = 50.0
            }
            admobBanner.adSize = kGADAdSizeSmartBannerPortrait
            admobBanner.adUnitID = ADMOB_AD_UNIT_ID_BANNER
            admobBanner.rootViewController = self
            admobBanner.loadRequest(GADRequest())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showInterstitialAd() {
        
        if interstitialAd != nil {
            if interstitialAd!.isReady {
                
                interstitialAd?.presentFromRootViewController(self)
                print("Ad presented")
            } else {
                print("Ad was not ready for presentation")
            }
        }
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let request = GADRequest()
        let interstitial = GADInterstitial(adUnitID: ADMOB_AD_UNIT_ID_INTERSTATIAL)
        interstitial.delegate = self
        interstitial.loadRequest(request)
        return interstitial
    }
    
    var stickerButt: UIButton?
    var stickerImage: UIImageView?
    
    /* Variables */
    var stickersTAGArray = [Int]()
    var sndrTAG = 0
    
    
    func setUpPreviousStickers()
    {
        
        
        for i in 0..<stickersImageArray.count{
            if (stickersImageArray[i] != ""){
                print("setmainstickersImageArray:\(stickersImageArray)")
                print("StickerPrevxCor:\(stickersImagePosition[i].frame)")
                print("StickerPrevyCor:\(stickersImagePosition[i].centrePoint)")
                
                stickerImage = UIImageView(frame: stickersImagePosition[i].frame)
                
                //stickerImage = UIImageView(frame: CGRectMake(0, 0, 200, 200))
                stickerImage?.center = stickersImagePosition[i].centrePoint
                
                stickerImage?.backgroundColor = UIColor.clearColor()
                stickerImage?.image = UIImage(named: stickersImageArray[i])
                stickerImage?.tag = i
                stickerImage?.userInteractionEnabled = true
                stickerImage?.multipleTouchEnabled = true
                containerView.addSubview(stickerImage!)
                
                
                // Add PAN GESTURES to the sticker
                let panGestSticker = UIPanGestureRecognizer()
                panGestSticker.addTarget(self, action: #selector(moveSticker(_:)))
                stickerImage!.addGestureRecognizer(panGestSticker)
                
                // Add DOUBLE TAP GESTURES to the sticker
                let doubletapGestSticker = UITapGestureRecognizer()
                doubletapGestSticker.addTarget(self, action: #selector(deleteSticker(_:)))
                doubletapGestSticker.numberOfTapsRequired = 2
                stickerImage!.addGestureRecognizer(doubletapGestSticker)
                
                // Add PINCH GESTURES to the sticker
                let pinchGestSticker = UIPinchGestureRecognizer()
                pinchGestSticker.addTarget(self, action: #selector(zoomSticker(_:)))
                stickerImage!.addGestureRecognizer(pinchGestSticker)
                
                
                // Add ROTATION GESTURES to the sticker
                let rotationGestSticker = UIRotationGestureRecognizer()
                rotationGestSticker.addTarget(self, action: #selector(rotateSticker(_:)))
                stickerImage!.addGestureRecognizer(rotationGestSticker)
                
                let singletapGeststicker = UITapGestureRecognizer()
                singletapGeststicker.addTarget(self, action: #selector(bringStickerToFront(_:)))
                singletapGeststicker.numberOfTapsRequired = 1
                stickerImage!.addGestureRecognizer(singletapGeststicker)
            }
            
            
            
        }
    
    }
    func setUpStickerTools() {
        stickerImage = UIImageView(frame: CGRectMake(0, 0, 150, 150))
        //stickerImage?.center = originalImage.center
        stickerImage?.center = CGPointMake(containerView.frame.size.width/2, containerView.frame.size.height/2)
//        stickerImage?.center = CGPointMake(screenWidth/2, screenHeight/2)
        print("Cont width:\(containerHolderView.frame.size.width)")
        print("Cont width:\(containerHolderView.frame.size.height)")
        stickerImage?.backgroundColor = UIColor.clearColor()
        stickerImage?.image = UIImage(named: (sticker?.imageName)!)
        stickerImage?.tag = stickersImageArray.count
        stickerImage?.userInteractionEnabled = true
        stickerImage?.multipleTouchEnabled = true
        containerView.addSubview(stickerImage!)
        
        //StickerPosition.imageName.append(sticker?.imageName)
        // Add an array of stickers
        stickersImageArray.append((sticker?.imageName)!)
        let stickerPosition = StickerPosition(frame: (stickerImage?.frame)!, centrePoint: (stickerImage?.center)! )
        
        stickersImagePosition.append(stickerPosition)
        
        
        print("setmainstickersImageArray:\(stickersImageArray)")
        
        
        // Add PAN GESTURES to the sticker
        let panGestSticker = UIPanGestureRecognizer()
        panGestSticker.addTarget(self, action: #selector(moveSticker(_:)))
        stickerImage!.addGestureRecognizer(panGestSticker)
        
        // Add DOUBLE TAP GESTURES to the sticker
        let doubletapGestSticker = UITapGestureRecognizer()
        doubletapGestSticker.addTarget(self, action: #selector(deleteSticker(_:)))
        doubletapGestSticker.numberOfTapsRequired = 2
        stickerImage!.addGestureRecognizer(doubletapGestSticker)
        
        // Add PINCH GESTURES to the sticker
        let pinchGestSticker = UIPinchGestureRecognizer()
        pinchGestSticker.addTarget(self, action: #selector(zoomSticker(_:)))
        stickerImage!.addGestureRecognizer(pinchGestSticker)
        
        
        // Add ROTATION GESTURES to the sticker
        let rotationGestSticker = UIRotationGestureRecognizer()
        rotationGestSticker.addTarget(self, action: #selector(rotateSticker(_:)))
        stickerImage!.addGestureRecognizer(rotationGestSticker)
        
        let singletapGeststicker = UITapGestureRecognizer()
        singletapGeststicker.addTarget(self, action: #selector(bringStickerToFront(_:)))
        singletapGeststicker.numberOfTapsRequired = 1
        stickerImage!.addGestureRecognizer(singletapGeststicker)
    }
    
    // MOVE STICKER
    func moveSticker(sender: UIPanGestureRecognizer) {
        let translation: CGPoint =  sender.translationInView(self.view)
        sender.view?.center = CGPointMake(sender.view!.center.x +  translation.x, sender.view!.center.y + translation.y)
        sender.setTranslation(CGPointMake(0, 0), inView: self.view)
        storeAdjustedPosition((sender.view?.frame)!, centre: (sender.view?.center)!, index: (sender.view?.tag)!
        )
    }
    // DELETE STICKER (with a double-tap on a sticker
    func deleteSticker(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
        //stickersTAGArray.removeLast()
        
        stickersImageArray[(sender.view?.tag)!] = ""
        //stickersImageArray.removeAtIndex((sender.view?.tag)!)
        
        
        
        //stickersImagePosition.removeLast()
        print("ondeletestickersImageArray:\(stickersImageArray)")
        
    }
    // ZOOM STICKER
    func zoomSticker(sender: UIPinchGestureRecognizer) {
        sender.view?.transform = CGAffineTransformScale(sender.view!.transform, sender.scale, sender.scale)
        sender.scale = 1
        storeAdjustedPosition((sender.view?.frame)!, centre: (sender.view?.center)!, index: (sender.view?.tag)!
        )
    }
    // ROTATE STICKER
    func rotateSticker(sender:UIRotationGestureRecognizer){
        if sender.state == UIGestureRecognizerState.Began ||
            sender.state == UIGestureRecognizerState.Changed  {
            sender.view!.transform = CGAffineTransformRotate(sender.view!.transform, sender.rotation)
            sender.rotation = 0
        }
    }
    
    func bringStickerToFront(sender: UILongPressGestureRecognizer){
        containerView.bringSubviewToFront(sender.view!)
    }
    
    func storeAdjustedPosition(frame: CGRect, centre: CGPoint, index: Int)  {
        stickersImagePosition[index].frame = frame
        stickersImagePosition[index].centrePoint = centre
    }

    var filterButt: UIButton?
    
    @IBAction func filterButtonTapped(sender: UIButton) {
        showToolView(FILTERSTOOL_NAME)
        hideToolView(ADJUSTMENTTOOL_NAME)
        //view.bringSubviewToFront(filtersToolView)
        
    }
    
    // Filters List & Names ------------------------------
    let filtersList = [
        "CIVignette",              //0
        "CIVignette",              //1
        "CIPhotoEffectInstant",    //2
        "CIPhotoEffectProcess",    //3
        "CIPhotoEffectTransfer",   //4
        "CISepiaTone",             //5
        "CIPhotoEffectChrome",     //6
        "CIPhotoEffectFade",       //7
        "CIPhotoEffectTonal",      //8
        "CIPhotoEffectNoir",       //9
        "CIDotScreen",             //10
        "CIColorPosterize",        //11
        "CISharpenLuminance",      //12
        "CIGammaAdjust",           //13
        
        // Add here new CIFilters...
    ]
    
    /*-----------------------------------------------*/
    
    
    func setupFiltersTool() {
        // Variables for setting the Buttons
        var xCoord: CGFloat = 0
        let yCoord: CGFloat = 3
        let buttonWidth:CGFloat = filtersScrollView.frame.size.height-6
        let buttonHeight: CGFloat = filtersScrollView.frame.size.height-6
        let gapBetweenButtons: CGFloat = 10
        
       
        
        // Loop for creating buttons =========================
        var itemCount = 0
        for i in 0..<filtersList.count {
            itemCount = i
            
            // Create a Button for each Texture ==========
            filterButt = UIButton(type: UIButtonType.Custom)
            filterButt?.frame = CGRectMake(xCoord, yCoord, buttonWidth, buttonHeight)
            filterButt?.tag = itemCount
            filterButt?.showsTouchWhenHighlighted = true
            filterButt?.addTarget(self, action: #selector(filterButtTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            //filterButt?.layer.cornerRadius = filterButt!.bounds.width/2
            filterButt?.clipsToBounds = true
            
            
            /*=================================================
             ================ FILTER SETTINGS ==================*/
            
            let ciContext = CIContext(options: nil)
            let coreImage = CIImage(image: originalImage.image!)
            let filter = CIFilter(name: "\(filtersList[itemCount])" )
            filter!.setDefaults()
            
            
            switch itemCount {
            case 0: // NO filter (don't edit this filter)
                break
                
            case 1: // Vignette
                filter!.setValue(3.0, forKey: kCIInputRadiusKey)
                filter!.setValue(4.0, forKey: kCIInputIntensityKey)
                break
                
            case 11: //Poster
                filter!.setValue(6.0, forKey: "inputLevels")
                break
                
            case 12: // Sharpen
                filter!.setValue(0.9, forKey: kCIInputSharpnessKey)
                break
                
            case 13: // Gamma Adjust
                filter!.setValue(3, forKey: "inputPower")
                break
                
                
                /* You can ddd new filters here,
                 Check Core Image Filter Reference here: https://developer.apple.com/library/mac/documentation/GraphicsImaging/Reference/CoreImageFilterReference/index.html
                 */
                
                
            default: break
            } // END FILTER SETTINGS =========================================
            
            
            
            filter!.setValue(coreImage, forKey: kCIInputImageKey)
            let filteredImageData = filter!.valueForKey(kCIOutputImageKey) as! CIImage
            let filteredImageRef = ciContext.createCGImage(filteredImageData, fromRect: filteredImageData.extent)
            let imageForButton = UIImage(CGImage: filteredImageRef);
            
            // Assign filtered image to the button
            filterButt!.setBackgroundImage(imageForButton, forState: UIControlState.Normal)
            
            // Add Buttons based on xCood
            xCoord +=  buttonWidth + gapBetweenButtons
            filtersScrollView.addSubview(filterButt!)
        } // END LOOP =========================================
        
        
        // Place Buttons into the ScrollView =====
        filtersScrollView.contentSize = CGSizeMake( (buttonWidth+gapBetweenButtons) * CGFloat(itemCount), yCoord)
    }
    
    func filterButtTapped(button: UIButton) {
        
        // Set the filteredImage
        imageForFilters.image = button.backgroundImageForState(UIControlState.Normal)
        
        // NO Filter (go back to Original image)
        if button.tag == 0 {   imageForFilters.image = originalImage.image  }
    }
    
    @IBAction func closeFiltersButton(sender: UIButton) {
        hideToolView(FILTERSTOOL_NAME)
    }
    
    
    
    
    // ANIMATION TO SHOW/HIDE TOOL VIEWS ================================
    func showToolView(toolName:String) {
        UIView.animateWithDuration(0.1, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            if toolName == FILTERSTOOL_NAME { self.filtersToolView.hidden = false }
            }, completion: { (finished: Bool) in
                
        });
    }
    
    func hideToolView(toolName:String) {
        UIView.animateWithDuration(0.1, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            if toolName == FILTERSTOOL_NAME { self.filtersToolView.hidden = true  }
            
            
            }, completion: { (finished: Bool) in
                
        });
    }
    @IBAction func pokifyButtonTapped(sender: UIButton) {
        
        
        if sender.tag == 1 {
            stickerType = "pokemon"
        }
        else if sender.tag == 2{
            stickerType = "emoji"
        }
        else if sender.tag == 3{
            stickerType = "facemask"
        }
        else if sender.tag == 4{
            stickerType = "se"
        }
        
        
        stickerPack = 0
        
        let stickerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("StickerViewController") as! StickerViewController
        
        
        self.addChildViewController(stickerVC)
        stickerVC.view.frame = self.view.frame
        self.view.addSubview(stickerVC.view)
        stickerVC.didMoveToParentViewController(self)
        
        
    }
    

    @IBAction func saveButtonTapped(sender: UIButton) {
        
        
        
        let rect:CGRect = containerView.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        containerView.drawViewHierarchyInRect(containerView.bounds, afterScreenUpdates: false)
        editedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        UIImageWriteToSavedPhotosAlbum(editedImage!, nil,nil, nil)
        simpleAlert("Your picture has been saved to Photo Library!")
    }
    @IBAction func homeButtonTapped(sender: UIButton) {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to exit? You'll lose all changes you've made so far.", preferredStyle: UIAlertControllerStyle.Alert)
        
        let action1 = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: returnHome)
        let action2 = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func returnHome(action: UIAlertAction){
        let homeVC = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        homeVC.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        self.presentViewController(homeVC, animated: true, completion: nil)
    }
    
    @IBAction func shareButtonTapped(sender: UIButton) {
        
        let hasPurchased = NSUserDefaults.standardUserDefaults().boolForKey("RemoveWatermarkAd")
        
        if hasPurchased {
            watermarkImg.hidden = true
        }
        else
        {
            watermarkImg.hidden = false
        }
         containerView.bringSubviewToFront(watermarkImg)
        showHUD()
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(snap_image), userInfo: nil, repeats: false)
        
       
        
        
    }
    
    func snap_image() {
        let rect:CGRect = containerView.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        containerView.drawViewHierarchyInRect(containerView.bounds, afterScreenUpdates: false)
        editedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        watermarkImg.hidden = true
        let shareVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("ShareViewController") as! ShareViewController
        
        
        self.addChildViewController(shareVC)
        shareVC.view.frame = self.view.frame
        self.view.addSubview(shareVC.view)
        shareVC.didMoveToParentViewController(self)
    }
    @IBAction func moreButtonTapped(sender: UIButton) {
        let moreVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MoreViewController") as! MoreViewController
        
        
        self.addChildViewController(moreVC)
        moreVC.view.frame = self.view.frame
        self.view.addSubview(moreVC.view)
        moreVC.didMoveToParentViewController(self)
    }
    
    override func willMoveToParentViewController(parent: UIViewController?) {
        print("Parent View")
    }
    
}
