//
//  HomeViewController.swift
//  Pokefy
//
//  Created by Saad on 7/24/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HomeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, GADInterstitialDelegate {

    var iMinSessions = 3
    var iTryAgainSessions = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenWidth = UIScreen.mainScreen().bounds.size.width
        screenHeight = UIScreen.mainScreen().bounds.size.height
        init_data()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(animated: Bool) {
        rateMe()
        interstitialAd = createAndLoadInterstitial()
        
        
        stickersImageArray.removeAll()
        stickersImagePosition.removeAll()
    }
    
    func init_data() {
        if(NSUserDefaults.standardUserDefaults().boolForKey("HasLaunchedOnce"))
        {
            // app already launched
            print("Already Launched")
            
        }
        else
        {
            // This is the first launch ever
            print("First Launch")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "HasLaunchedOnce")
            //In App Purchase Initialization
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PokemonPack1")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PokemonPack2")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PokemonPack3")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PokemonPack4")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PokemonPackAll")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "FacemaskPack1")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "SpecialEffectsPack1")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "SpecialEffectsPack2")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "AllinPack")
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "RemoveWatermarkAd")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [NSObject : AnyObject]?) {
        takenImage = image
        self.dismissViewControllerAnimated(false, completion: nil)
        goToCropImageVC()
    }
    
    func goToCropImageVC() {
        
        
        let ciVC = self.storyboard?.instantiateViewControllerWithIdentifier("CropViewController") as! CropViewController
        presentViewController(ciVC, animated: true, completion: nil)
    }

    @IBAction func moreAppsButtonTapped(sender: UIButton) {
       showInterstitialAd()
    }
    @IBAction func purchaseButtonTapped(sender: UIButton) {
        let iapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("InAppPurchaseViewController") as! InAppPurchaseViewController
        
        
        self.addChildViewController(iapVC)
        iapVC.view.frame = self.view.frame
        self.view.addSubview(iapVC.view)
        iapVC.didMoveToParentViewController(self)
    }
    @IBAction func rateButtonTapped(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: APP_STORE_LINK)!)
    }
    @IBAction func cameraButtonTapped(sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        
    }
    @IBAction func libraryButtonTapped(sender: UIButton) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePicker.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    func rateMe() {
        let neverRate = NSUserDefaults.standardUserDefaults().boolForKey("neverRate")
        var numLaunches = NSUserDefaults.standardUserDefaults().integerForKey("numLaunches") + 1
        
        if (!neverRate && (numLaunches == iMinSessions || numLaunches >= (iMinSessions + iTryAgainSessions + 1)))
        {
            showRateMe()
            numLaunches = iMinSessions + 1
        }
        NSUserDefaults.standardUserDefaults().setInteger(numLaunches, forKey: "numLaunches")
    }
    
    func showRateMe() {
        let alert = UIAlertController(title: "Rate Us", message: "Do you love this app?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Rate it Now!", style: UIAlertActionStyle.Default, handler: { alertAction in
            UIApplication.sharedApplication().openURL(NSURL(string : APP_STORE_LINK)!)
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No Thanks", style: UIAlertActionStyle.Default, handler: { alertAction in
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "neverRate")
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Maybe Later", style: UIAlertActionStyle.Default, handler: { alertAction in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let request = GADRequest()
        let interstitial = GADInterstitial(adUnitID: ADMOB_AD_UNIT_ID_INTERSTATIAL)
        interstitial.delegate = self
        interstitial.loadRequest(request)
        return interstitial
    }
    
    func showInterstitialAd() {
        
        if interstitialAd != nil {
            if interstitialAd!.isReady {
                interstitialAd?.presentFromRootViewController(self)
                print("Ad presented")
            } else {
                print("Ad was not ready for presentation")
            }
        }
    }
    
}
