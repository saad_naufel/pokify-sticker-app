//
//  StickerCollectionViewCell.swift
//  Pokefy
//
//  Created by Saad on 8/21/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit

class StickerCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet var stickerButton: UIButton!
    
    @IBOutlet var sticketImage: UIImageView!
    var sticker:Sticker? {
        didSet {
            if let sticker = sticker {
                sticketImage.image = UIImage(named: sticker.imageName)
                
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        sticketImage.image = nil
    }
    
    
    
}
