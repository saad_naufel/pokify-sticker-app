//
//  StickerViewController.swift
//  Pokefy
//
//  Created by Saad on 8/17/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit

class StickerViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    private var stickerDataSource = StickersDataSource()
    private let reuseIdentifier = "StickerCollectionCell"
    @IBOutlet var popUpBgView: UIView!
    
    @IBOutlet var lockButton: UIButton!
    @IBOutlet var lockImage: UIImageView!
    @IBOutlet var pack4Button: UIButton!
    @IBOutlet var pack3Button: UIButton!
    @IBOutlet var stickerCollectionView: UICollectionView!
    
    @IBOutlet var pack2Button: UIButton!
    @IBOutlet var freeButton: UIButton!
    
    @IBOutlet var pack1Button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.7)
        
        // adding the blur effect
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = popUpBgView.bounds
        popUpBgView.insertSubview(blurView, atIndex: 0)
        
        self.showAnimate()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePopUp(sender: UIButton) {
       self.removeAnimate()
    }
    
    override func viewDidLayoutSubviews() {
        
    }

    override func viewWillAppear(animated: Bool) {
        
        if stickerType == "emoji" {
            pack1Button.hidden = true
            pack2Button.hidden = true
            pack3Button.hidden = true
            pack4Button.hidden = true
        }
        else if stickerType == "facemask" {
            pack2Button.hidden = true
            pack3Button.hidden = true
            pack4Button.hidden = true
        }
        else if stickerType == "se" {
            pack3Button.hidden = true
            pack4Button.hidden = true
        }
        
        lockImage.hidden = true
        lockButton.hidden = true
        alternate_button_bg_and_interaction(freeButton, state: "active")
        alternate_button_bg_and_interaction(pack1Button, state: "notactive")
        alternate_button_bg_and_interaction(pack2Button, state: "notactive")
        alternate_button_bg_and_interaction(pack3Button, state: "notactive")
        alternate_button_bg_and_interaction(pack4Button, state: "notactive")
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stickerDataSource.count
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSizeMake((screenWidth * 0.90) / 4.0, ((screenHeight * 0.71) * 0.90) / 5.0)
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("StickerCollectionCell", forIndexPath: indexPath) as! StickerCollectionViewCell
        
        //print(indexPath)
        if let sticker = stickerDataSource.stickerForItemAtIndexPath(indexPath){
            cell.sticker = sticker
        }
        
        
        return cell
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func freeButtonTapped(sender: UIButton) {
        lockImage.hidden = true
        lockButton.hidden = true
        stickerCollectionView.userInteractionEnabled = true
        stickerPack = 0
        stickerDataSource = StickersDataSource()
        UIView.transitionWithView(self.stickerCollectionView, duration: 0.33, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: {
            self.stickerCollectionView.reloadData()
            }, completion: nil)
        
        alternate_button_bg_and_interaction(freeButton, state: "active")
        alternate_button_bg_and_interaction(pack1Button, state: "deactive")
        alternate_button_bg_and_interaction(pack2Button, state: "deactive")
        alternate_button_bg_and_interaction(pack3Button, state: "deactive")
        alternate_button_bg_and_interaction(pack4Button, state: "deactive")
    }
    @IBAction func pack1ButtonTapped(sender: UIButton) {
        
        
        var locked = !NSUserDefaults.standardUserDefaults().boolForKey("AllinPack")
        
        if stickerType == "pokemon" {
            if (NSUserDefaults.standardUserDefaults().boolForKey("PokemonPack1") || NSUserDefaults.standardUserDefaults().boolForKey("PokemonPackAll"))
            {
                locked = false
            }
        }
        if stickerType == "facemask" {
            if (NSUserDefaults.standardUserDefaults().boolForKey("FacemaskPack1"))
            {
                locked = false
            }
        }
        if stickerType == "se" {
            if (NSUserDefaults.standardUserDefaults().boolForKey("SpecialEffectsPack1"))
            {
                locked = false
            }
        }
        
        
        
        if locked {
           lockImage.hidden = false
            lockButton.hidden = false
            stickerCollectionView.userInteractionEnabled = false
        }
        else
        {
            lockImage.hidden = true
            lockButton.hidden = true
            stickerCollectionView.userInteractionEnabled = true
        }
        
        stickerPack = 1
        stickerDataSource = StickersDataSource()
        UIView.transitionWithView(self.stickerCollectionView, duration: 0.33, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: {
            self.stickerCollectionView.reloadData()
            }, completion: nil)
        
        alternate_button_bg_and_interaction(pack1Button, state: "active")
        alternate_button_bg_and_interaction(freeButton, state: "deactive")
        alternate_button_bg_and_interaction(pack2Button, state: "deactive")
        alternate_button_bg_and_interaction(pack3Button, state: "deactive")
        alternate_button_bg_and_interaction(pack4Button, state: "deactive")
        
    }
    
    @IBAction func pack2ButtonTapped(sender: UIButton) {
        var locked = !NSUserDefaults.standardUserDefaults().boolForKey("AllinPack")
        
        if stickerType == "pokemon" {
            if (NSUserDefaults.standardUserDefaults().boolForKey("PokemonPack2") || NSUserDefaults.standardUserDefaults().boolForKey("PokemonPackAll"))
            {
                locked = false
            }
        }
        if stickerType == "se" {
            if (NSUserDefaults.standardUserDefaults().boolForKey("SpecialEffectsPack2"))
            {
                locked = false
            }
        }
        
        
        if locked {
            lockImage.hidden = false
            lockButton.hidden = false
            stickerCollectionView.userInteractionEnabled = false
        }
        else
        {
            lockImage.hidden = true
            lockButton.hidden = true
            stickerCollectionView.userInteractionEnabled = true
        }
        stickerPack = 2
        stickerDataSource = StickersDataSource()
        UIView.transitionWithView(self.stickerCollectionView, duration: 0.33, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: {
            self.stickerCollectionView.reloadData()
            }, completion: nil)
        alternate_button_bg_and_interaction(pack2Button, state: "active")
        alternate_button_bg_and_interaction(freeButton, state: "deactive")
        alternate_button_bg_and_interaction(pack1Button, state: "deactive")
        alternate_button_bg_and_interaction(pack3Button, state: "deactive")
        alternate_button_bg_and_interaction(pack4Button, state: "deactive")

    }
    @IBAction func pack3ButtonTapped(sender: UIButton) {
        var locked = !NSUserDefaults.standardUserDefaults().boolForKey("AllinPack")
        
        if stickerType == "pokemon" {
            if (NSUserDefaults.standardUserDefaults().boolForKey("PokemonPack3") || NSUserDefaults.standardUserDefaults().boolForKey("PokemonPackAll"))
            {
                locked = false
            }
        }
        
        
        if locked {
            lockImage.hidden = false
            lockButton.hidden = false
            stickerCollectionView.userInteractionEnabled = false
        }
        else
        {
            lockImage.hidden = true
            lockButton.hidden = true
            stickerCollectionView.userInteractionEnabled = true
        }
        stickerPack = 3
        stickerDataSource = StickersDataSource()
        UIView.transitionWithView(self.stickerCollectionView, duration: 0.33, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: {
            self.stickerCollectionView.reloadData()
            }, completion: nil)
        alternate_button_bg_and_interaction(pack3Button, state: "active")
        alternate_button_bg_and_interaction(pack1Button, state: "deactive")
        alternate_button_bg_and_interaction(pack2Button, state: "deactive")
        alternate_button_bg_and_interaction(freeButton, state: "deactive")
        alternate_button_bg_and_interaction(pack4Button, state: "deactive")
    }
    @IBAction func pack4ButtonTapped(sender: UIButton) {
        var locked = !NSUserDefaults.standardUserDefaults().boolForKey("AllinPack")
        
        if stickerType == "pokemon" {
            if (NSUserDefaults.standardUserDefaults().boolForKey("PokemonPack4") || NSUserDefaults.standardUserDefaults().boolForKey("PokemonPackAll"))
            {
                locked = false
            }
        }
        
        
        if locked {
            lockImage.hidden = false
            lockButton.hidden = false
            stickerCollectionView.userInteractionEnabled = false
        }
        else
        {
            lockImage.hidden = true
            lockButton.hidden = true
            stickerCollectionView.userInteractionEnabled = true
        }
        stickerPack = 4
        stickerDataSource = StickersDataSource()
        UIView.transitionWithView(self.stickerCollectionView, duration: 0.33, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: {
            self.stickerCollectionView.reloadData()
            }, completion: nil)
        alternate_button_bg_and_interaction(pack4Button, state: "active")
        alternate_button_bg_and_interaction(pack1Button, state: "deactive")
        alternate_button_bg_and_interaction(pack2Button, state: "deactive")
        alternate_button_bg_and_interaction(pack3Button, state: "deactive")
        alternate_button_bg_and_interaction(freeButton, state: "deactive")
    }
    func alternate_button_bg_and_interaction(button: UIButton, state: String){
        
        if state == "active" {
            button.setBackgroundImage(UIImage(named: "pack_btn_activebg.png"), forState: UIControlState.Normal)
            button.userInteractionEnabled = false
        }
        else{
        button.setBackgroundImage(UIImage(named: "pack_button_bg_pokemon.png"), forState: UIControlState.Normal)
            button.userInteractionEnabled = true
        }
        
    }
    
    
    
    @IBAction func lockButtonTapped(sender: UIButton) {
        let iapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("InAppPurchaseViewController") as! InAppPurchaseViewController
        
        
        self.addChildViewController(iapVC)
        iapVC.view.frame = self.view.frame
        self.view.addSubview(iapVC.view)
        iapVC.didMoveToParentViewController(self)
    }
    func getIndexpath(item: Int) -> NSIndexPath {
        return NSIndexPath(index: 0)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let sticker = stickerDataSource.stickerForItemAtIndexPath(indexPath) {
            performSegueWithIdentifier("StickerToEditor", sender: sticker)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "StickerToEditor" {
            let imageEditorVC = segue.destinationViewController as! ImageEditorViewController
            imageEditorVC.sticker = sender as? Sticker
        }
    }

}

