//
//  MoreViewController.swift
//  Pokefy
//
//  Created by Saad on 8/28/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import Social
import GoogleMobileAds

class MoreViewController: UIViewController, GADInterstitialDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.showAnimate()
    }

    override func viewWillAppear(animated: Bool) {
        interstitialAd = createAndLoadInterstitial()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func premiumContentsTapped(sender: UIButton) {
    }

    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    @IBAction func rateUsTapped(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: APP_STORE_LINK)!)
    }
    @IBAction func tellAFriendTapped(sender: UIButton) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            let facebookSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.addURL(NSURL(string: APP_STORE_LINK))
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        }
    }
    @IBAction func premiumContentTapped(sender: UIButton) {
        //self.view.removeFromSuperview()
        let iapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("InAppPurchaseViewController") as! InAppPurchaseViewController
        
        
        self.addChildViewController(iapVC)
        iapVC.view.frame = self.view.frame
        self.view.addSubview(iapVC.view)
        iapVC.didMoveToParentViewController(self)
        
    }
    
    @IBAction func returnToHomeTapped(sender: UIButton) {
        
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to exit? You'll lose all changes you've made so far.", preferredStyle: UIAlertControllerStyle.Alert)
        
        let action1 = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: returnHome)
        let action2 = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler: nil)
        
        alert.addAction(action1)
        alert.addAction(action2)
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    func returnHome(action: UIAlertAction){
        let homeVC = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        homeVC.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        self.presentViewController(homeVC, animated: true, completion: nil)
    }
   
    @IBAction func restorePurchaseTapped(sender: UIButton) {
        let iapVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("InAppPurchaseViewController") as! InAppPurchaseViewController
        
        
        self.addChildViewController(iapVC)
        iapVC.view.frame = self.view.frame
        self.view.addSubview(iapVC.view)
        iapVC.didMoveToParentViewController(self)
    }
    @IBAction func moreAppsTappled(sender: UIButton) {
        showInterstitialAd()
    }
    @IBAction func moreCloseTapped(sender: UIButton) {
        self.removeAnimate()
    }
    func createAndLoadInterstitial() -> GADInterstitial {
        let request = GADRequest()
        let interstitial = GADInterstitial(adUnitID: ADMOB_AD_UNIT_ID_INTERSTATIAL)
        interstitial.delegate = self
        interstitial.loadRequest(request)
        return interstitial
    }
    
    func showInterstitialAd() {
        
        if interstitialAd != nil {
            if interstitialAd!.isReady {
                interstitialAd?.presentFromRootViewController(self)
                print("Ad presented")
            } else {
                print("Ad was not ready for presentation")
            }
        }
    }
}
