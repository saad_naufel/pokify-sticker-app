//
//  InAppPurchaseViewController.swift
//  Pokefy
//
//  Created by Saad on 8/29/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import StoreKit

class InAppPurchaseViewController: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    @IBOutlet var popUpViewBg: UIView!
    
    var products = [SKProduct]()
    var busyView: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // adding the blur effect
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = popUpViewBg.bounds
        popUpViewBg.insertSubview(blurView, atIndex: 0)
        
        self.showAnimate()
        
        
        
        
        busyView = UIActivityIndicatorView(frame: CGRectMake(0.0, 0.0, UIScreen.mainScreen().bounds.size.width, screenHeight))
        busyView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        busyView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        self.view.addSubview(busyView)
        
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        
        busyView.startAnimating()
        
        
        requestProductInformation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func requestProductInformation() {
        if SKPaymentQueue.canMakePayments() {
            products = [SKProduct]()
            let productIDs = Set(["2004.PokemonPack1", "2004.PokemonPack2", "2004.PokemonPack3", "2004.PokemonPack4", "2004.PokemonPackAll", "2004.FacemaskPack1", "2004.SpecialEffectsPack1", "2004.SpecialEffectsPack2", "2004.AllinPack", "2004.RemoveWatermarkAd"])
            let productRequest = SKProductsRequest(productIdentifiers: productIDs)
            productRequest.delegate = self
            productRequest.start()
        }
        else {
            showAlert("", alertMessage: "In app purchase disabled")
        }
    }
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        busyView.stopAnimating()
        products = response.products
        if products.count == 0 {
            showAlert("", alertMessage: "No products found")
        }
    }
    
    func request(request: SKRequest, didFailWithError error: NSError) {
        busyView.stopAnimating()
        showAlert("", alertMessage: error.localizedDescription)
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    @IBAction func productButtonTapped(sender: UIButton) {
        if products.count == 0 {
            showAlert("", alertMessage: "No products to purchase")
        }
        else {
            busyView.startAnimating()
            var index = 0
            for i in 0 ..< products.count {
                if sender.tag == 1 && products[i].productIdentifier == "2004.PokemonPack1" {
                    index = i
                    break
                }
                if sender.tag == 2 && products[i].productIdentifier == "2004.PokemonPack2" {
                    index = i
                    break
                }
                if sender.tag == 3 && products[i].productIdentifier == "2004.PokemonPack3" {
                    index = i
                    break
                }
                if sender.tag == 4 && products[i].productIdentifier == "2004.PokemonPack4" {
                    index = i
                    break
                }
                if sender.tag == 5 && products[i].productIdentifier == "2004.PokemonPackAll" {
                    index = i
                    break
                }
                if sender.tag == 6 && products[i].productIdentifier == "2004.FacemaskPack1" {
                    index = i
                    break
                }
                if sender.tag == 8 && products[i].productIdentifier == "2004.SpecialEffectsPack1" {
                    index = i
                    break
                }
                if sender.tag == 9 && products[i].productIdentifier == "2004.SpecialEffectsPack2" {
                    index = i
                    break
                }
                if sender.tag == 10 && products[i].productIdentifier == "2004.AllinPack" {
                    index = i
                    break
                }
                if sender.tag == 11 && products[i].productIdentifier == "2004.RemoveWatermarkAd" {
                    index = i
                    break
                }
            }
            let payment = SKPayment(product: products[index])
            SKPaymentQueue.defaultQueue().addPayment(payment)
        }
    }

    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions as [SKPaymentTransaction] {
            switch transaction.transactionState {
            case SKPaymentTransactionState.Purchased:
                completeTransaction(transaction)
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                busyView.stopAnimating()
                
            case SKPaymentTransactionState.Failed:
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                showAlert("", alertMessage: "Transaction failed")
                busyView.stopAnimating()
                
            case SKPaymentTransactionState.Restored:
                completeTransaction(transaction)
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                busyView.stopAnimating()
                showAlert("", alertMessage: "Restored Purchase:\(transaction.payment.productIdentifier)")
            default:
                break
            }
        }
    }
    
    func completeTransaction(transaction: SKPaymentTransaction)
    {
        if transaction.payment.productIdentifier == "2004.PokemonPack1" {
            deliverProduct("PokemonPack1")
        }
        else if transaction.payment.productIdentifier == "2004.PokemonPack2" {
            deliverProduct("PokemonPack2")
        }
        else if transaction.payment.productIdentifier == "2004.PokemonPack3" {
            deliverProduct("PokemonPack3")
        }
        else if transaction.payment.productIdentifier == "2004.PokemonPack4" {
            deliverProduct("PokemonPack4")
        }
        else if transaction.payment.productIdentifier == "2004.PokemonPackAll" {
            deliverProduct("PokemonPackAll")
        }
        else if transaction.payment.productIdentifier == "2004.FacemaskPack1" {
            deliverProduct("FacemaskPack1")
        }
        else if transaction.payment.productIdentifier == "2004.SpecialEffectsPack1" {
            deliverProduct("SpecialEffectsPack1")
        }
        else if transaction.payment.productIdentifier == "2004.SpecialEffectsPack2" {
            deliverProduct("SpecialEffectsPack2")
        }
        else if transaction.payment.productIdentifier == "2004.AllinPack" {
            deliverProduct("AllinPack")
        }
        else if transaction.payment.productIdentifier == "2004.RemoveWatermarkAd" {
            deliverProduct("RemoveWatermarkAd")
        }
        
    }
    
    func deliverProduct(defaultsKey: String) {
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: defaultsKey)
    }
    
    
    
    @IBAction func closeButtontapped(sender: UIButton) {
        self.removeAnimate()
    }
    func showAlert(alertTitle: String, alertMessage: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func restorePurchaseTapped(sender: UIButton) {
        busyView.startAnimating()
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }
}
