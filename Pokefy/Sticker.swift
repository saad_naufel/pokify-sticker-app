//
//  Sticker.swift
//  Pokefy
//
//  Created by Saad on 8/21/16.
//  Copyright © 2016 Saad. All rights reserved.
//


import UIKit




class Sticker {
    
    var imageName: String
    var pack: Int
    var type: String
    
    init(imageName:String, pack: Int, type: String) {
        self.imageName = imageName
        self.pack = pack
        self.type = type
        
    }
    
    convenience init(copying sticker: Sticker) {
        self.init(imageName: sticker.imageName, pack: sticker.pack, type: sticker.type)
    }
    
}


class StickerPosition{
    var frame: CGRect
    var centrePoint: CGPoint
    
    init(frame:CGRect, centrePoint: CGPoint){
        self.frame = frame
        self.centrePoint = centrePoint
    }

}
