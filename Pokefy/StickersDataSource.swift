

import UIKit

class StickersDataSource {
    
    private var stickers: [Sticker] = []
    private var immutableStickers: [Sticker] = []
    
    var count: Int {
        return stickers.count
    }
    
    
    
    // MARK: Public
    
    init() {
        stickers = loadStickers(stickerType!, pack: stickerPack!)
        immutableStickers = stickers
    }
    
    
    func indexPathForNewRandomPaper(new_stickers: [Sticker], newIndex: Int) -> NSIndexPath {
        let index = Int(newIndex)
        let stickerToCopy = new_stickers[index]
        let newSticker = Sticker(copying: stickerToCopy)
        stickers.append(newSticker)
        //stickers.sortInPlace { $0.index < $1.index }
        return indexPathForPaper(newSticker)
    }
    
    func indexPathForPaper(sticker: Sticker) -> NSIndexPath {
        
        return NSIndexPath(forItem: 0, inSection: 0)
    }
    
    func stickerForItemAtIndexPath(indexPath: NSIndexPath) -> Sticker? {
        
            return stickers[indexPath.item]
    }
    
    
    func deleteItemsAtIndexPaths(indexPaths: [NSIndexPath]) {
        var indexes: [Int] = []
        for indexPath in indexPaths {
            indexes.append(absoluteIndexForIndexPath(indexPath))
        }
        var newStickers: [Sticker] = []
        for (index, sticker) in stickers.enumerate() {
            if !indexes.contains(index) {
                newStickers.append(sticker)
            }
        }
        stickers = newStickers
    }
    
    func numberOfPapersInSection(index: Int) -> Int {
        return stickers.count
    }
    

    // MARK: Private
    
    private func absoluteIndexForIndexPath(indexPath: NSIndexPath) -> Int {
        var index = 0
        for i in 0..<indexPath.section {
            index += numberOfPapersInSection(i)
        }
        index += indexPath.item
        return index
    }
    
    func loadStickers(type: String, pack: Int) -> [Sticker] {
        var packname: String
        
        if(type == "pokemon" && pack == 0 ){
            packname = "pokemon_free"
        }
        else if(type == "pokemon" && pack == 1 ){
            packname = "pokemon_paid_1"
        }
        else if(type == "pokemon" && pack == 2 ){
            packname = "pokemon_paid_2"
        }
        else if(type == "pokemon" && pack == 3 ){
            packname = "pokemon_paid_3"
        }
        else if(type == "pokemon" && pack == 4 ){
            packname = "pokemon_paid_4"
        }
        else if(type == "emoji" && pack == 0 ){
            packname = "emoji_free"
        }
        else if(type == "facemask" && pack == 0 ){
            packname = "facemask_free"
        }
        else if(type == "facemask" && pack == 1 ){
            packname = "facemask_paid_1"
        }
        else if(type == "se" && pack == 0 ){
            packname = "special_effects_free"
        }
        else if(type == "se" && pack == 1 ){
            packname = "special_effects_paid_1"
        }
        else if(type == "se" && pack == 2 ){
            packname = "special_effects_paid_2"
        }
        else{
            packname = "pokemon_free"
        }
        
        if let path = NSBundle.mainBundle().pathForResource(packname, ofType: "plist") {
            if let dictArray = NSArray(contentsOfFile: path) {
                var stickers: [Sticker] = []
                for item in dictArray {
                    if let dict = item as? NSDictionary {
                        let imageName = dict["imageName"] as! String
                        let pack = dict["pack"] as! Int
                        let type = dict["type"] as! String
                        let sticker = Sticker(imageName: imageName, pack: pack, type: type)
                        
                        stickers.append(sticker)
                    }
                }
                return stickers
            }
        }
        return []
    }
    
    
    
    
    
}

