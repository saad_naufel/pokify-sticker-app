//
//  CropViewController.swift
//  Pokefy
//
//  Created by Saad on 8/14/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import GoogleMobileAds

class CropViewController: UIViewController, GADInterstitialDelegate {

    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var bannerView: GADBannerView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var bkgImage: UIImageView!
    @IBOutlet var imageToBeCropped: UIImageView!
    @IBOutlet var imageToBeCroppedWidth: NSLayoutConstraint!
    @IBOutlet var imageToBeCroppedHeight: NSLayoutConstraint!
    
    @IBOutlet var cropbarImage: UIImageView!
    @IBOutlet var imageToBeCroppedAspectConstraints: NSLayoutConstraint!
    
    @IBOutlet var gadBannerHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        interstitialAd = createAndLoadInterstitial()
        //showInterstitialAd()
        
        _ = changeMultiplier(imageToBeCroppedAspectConstraints, multiplier: (takenImage?.size.width)! / (takenImage?.size.height)!)
        
        imageToBeCropped.image = takenImage
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        init_ad()
    }
    
    func init_ad() {
        let hasPurchased = NSUserDefaults.standardUserDefaults().boolForKey("RemoveWatermarkAd")
        
        if hasPurchased {
            bannerView.hidden = true
            bottomLayoutConstraint.constant = 0.0
            
        }
        else
        {
            
            bannerView.hidden = false
            if (UI_USER_INTERFACE_IDIOM() == .Pad) {
                bottomLayoutConstraint.constant = 90.0
                gadBannerHeight.constant = 90.0
            }
            else
            {
                bottomLayoutConstraint.constant = 50.0
                gadBannerHeight.constant = 50.0
            }
            bannerView.adSize = kGADAdSizeSmartBannerPortrait
            bannerView.adUnitID = ADMOB_AD_UNIT_ID_BANNER
            bannerView.rootViewController = self
            bannerView.loadRequest(GADRequest())
        }
    }
    
    @IBAction func cropButtonTapped(sender: UIButton) {
        cropbarImage.hidden = true
        showHUD()
        NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(cropImage), userInfo: nil, repeats: false)
    }

    func showInterstitialAd() {
        
        if interstitialAd != nil {
            if interstitialAd!.isReady {
                interstitialAd?.presentFromRootViewController(self)
                print("Ad presented")
            } else {
                print("Ad was not ready for presentation")
            }
        }
    }
    
    func cropImage() {
        
        
        let rect:CGRect = containerView.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)
        containerView.drawViewHierarchyInRect(containerView.bounds, afterScreenUpdates: false)
        croppedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        let imageEditorVC = self.storyboard?.instantiateViewControllerWithIdentifier("ImageEditorViewController") as! ImageEditorViewController
        presentViewController(imageEditorVC, animated: true, completion: nil)
        
        
        
        
    }
    
    
    func changeMultiplier(constraint: NSLayoutConstraint, multiplier: CGFloat) -> NSLayoutConstraint {
        
        let newConstraint = NSLayoutConstraint(
            item: constraint.firstItem,
            attribute: constraint.firstAttribute,
            relatedBy: constraint.relation,
            toItem: constraint.secondItem,
            attribute: constraint.secondAttribute,
            multiplier: multiplier,
            constant: constraint.constant
        )
        
        newConstraint.priority = constraint.priority
        
        NSLayoutConstraint.deactivateConstraints([constraint])
        NSLayoutConstraint.activateConstraints([newConstraint])
        print(newConstraint.multiplier)
        return newConstraint
        
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let request = GADRequest()
        let interstitial = GADInterstitial(adUnitID: ADMOB_AD_UNIT_ID_INTERSTATIAL)
        interstitial.delegate = self
        interstitial.loadRequest(request)
        return interstitial
    }
    
    

    
    @IBAction func moveImage(sender: UIPanGestureRecognizer) {
        //print("Panned")
        let translation: CGPoint =  sender.translationInView(self.view)
        sender.view?.center = CGPointMake(sender.view!.center.x +  translation.x, sender.view!.center.y + translation.y)
        sender.setTranslation(CGPointMake(0, 0), inView: self.view)
    }
    @IBAction func zoomImage(sender: UIPinchGestureRecognizer) {
        sender.view?.transform = CGAffineTransformScale(sender.view!.transform, sender.scale, sender.scale)
        sender.scale = 1
    }

    @IBAction func rotateImage(sender: UIRotationGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.Began ||
            sender.state == UIGestureRecognizerState.Changed {
            sender.view!.transform = CGAffineTransformRotate(sender.view!.transform, sender.rotation)
            sender.rotation = 0
        }
    }
    @IBAction func backButtonTapped(sender: UIButton) {
        let homeVC = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        homeVC.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        self.presentViewController(homeVC, animated: true, completion: nil)
    }
    
    @IBAction func moreButtonTapped(sender: UIButton) {
        let moreVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MoreViewController") as! MoreViewController
        
        
        self.addChildViewController(moreVC)
        moreVC.view.frame = self.view.frame
        self.view.addSubview(moreVC.view)
        moreVC.didMoveToParentViewController(self)
    }

}
