//
//  ShareViewController.swift
//  Pokefy
//
//  Created by Saad on 8/22/16.
//  Copyright © 2016 Saad. All rights reserved.
//

import UIKit
import Social
import MessageUI


class ShareViewController: UIViewController, MFMailComposeViewControllerDelegate, UIDocumentInteractionControllerDelegate{

    @IBOutlet var previewImage: UIImageView!
    @IBOutlet var popUpBgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // adding the blur effect
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = popUpBgView.bounds
        popUpBgView.insertSubview(blurView, atIndex: 0)
        previewImage.image = editedImage
        
        self.showAnimate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    @IBAction func closeButtonTapped(sender: UIButton) {
        self.removeAnimate()
    }

    @IBAction func fbShareButtonTapped(sender: UIButton) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            let fbSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            fbSheet.setInitialText(SHARING_MESSAGE)
            fbSheet.addImage(editedImage)
            self.presentViewController(fbSheet, animated: true, completion: nil)
        } else {
            
            showAlert("Facebook", alertMessage: "Please login to your Facebook account in Settings")
        }
    }

    @IBAction func twitterShareTapped(sender: UIButton) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter) {
            let twSheet = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twSheet.setInitialText(SHARING_MESSAGE)
            twSheet.addImage(editedImage)
            self.presentViewController(twSheet, animated: true, completion: nil)
        } else {
            
            showAlert("Twitter", alertMessage: "Please login to your Twitter account in Settings")
        }
    }
    var docIntController = UIDocumentInteractionController()
    @IBAction func instagramShareTapped(sender: UIButton) {
//        let instagramURL = NSURL(string: "instagram://app")!
//        if UIApplication.sharedApplication().canOpenURL(instagramURL) {
        
            //Save the Image to default device Directory
            let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            let savedImagePath:String = paths.stringByAppendingString("/image.igo")
            let imageData: NSData = UIImageJPEGRepresentation(editedImage!, 1.0)!
            imageData.writeToFile(savedImagePath, atomically: false)
            
            //Load the Image Path
            let getImagePath = paths.stringByAppendingString("/image.igo")
            let fileURL: NSURL = NSURL.fileURLWithPath(getImagePath)
            
            
            docIntController = UIDocumentInteractionController(URL: fileURL)
            docIntController.UTI = "com.instagram.exclusivegram"
            docIntController.delegate = self
            docIntController.presentOpenInMenuFromRect(CGRectZero, inView: self.view, animated: true)
            
//        } else {
//            let alert:UIAlertView = UIAlertView(title: APP_NAME,
//                                                message: "Instagram not found, please download it on the App Store",
//                                                delegate: nil,
//                                                cancelButtonTitle: "OK")
//            alert.show()
//        }
    }
    @IBAction func pinterestShareTapped(sender: UIButton) {
    }
    @IBAction func mailShareTapped(sender: UIButton) {
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject(SHARING_SUBJECT)
        mailComposer.setMessageBody(SHARING_MESSAGE, isHTML: true)
        
        // Attach image
        let imageData = UIImageJPEGRepresentation(editedImage!, 1.0)
        mailComposer.addAttachmentData(imageData!, mimeType: "image/png", fileName: "MyPhoto.png")
        
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposer, animated: true, completion: nil)
        }
        
    }
    
    // Email delegate
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError?) {
        var outputMessage = ""
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            outputMessage = "Mail cancelled"
        case MFMailComposeResultSaved.rawValue:
            outputMessage = "Mail saved"
        case MFMailComposeResultSent.rawValue:
            outputMessage = "Mail sent"
        case MFMailComposeResultFailed.rawValue:
            outputMessage = "Something went wrong with sending Mail, try again later."
        default: break
        }
        
        showAlert(APP_NAME, alertMessage: outputMessage)
        
        dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func moreShareTapped(sender: UIButton) {
        // NOTE: The following method works only on a Real device, not on iOS Simulator, + You should have apps like Instagram, iPhoto, etc. already installed into your device!
        
        //Save the Image to default device Directory
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let savedImagePath:String = paths.stringByAppendingString("/image.jpg")
        let imageData: NSData = UIImageJPEGRepresentation(editedImage!, 1.0)!
        imageData.writeToFile(savedImagePath, atomically: false)
        
        //Load the Image Path
        let getImagePath = paths.stringByAppendingString("/image.jpg")
        let fileURL: NSURL = NSURL.fileURLWithPath(getImagePath)
        
        // Open the Document Interaction controller for Sharing options
        docIntController.delegate = self
        docIntController = UIDocumentInteractionController(URL: fileURL)
        docIntController.presentOpenInMenuFromRect(CGRectZero, inView: self.view, animated: true)
    }
    @IBAction func moreButtonTapped(sender: UIButton) {
        let moreVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MoreViewController") as! MoreViewController
        
        
        self.addChildViewController(moreVC)
        moreVC.view.frame = self.view.frame
        self.view.addSubview(moreVC.view)
        moreVC.didMoveToParentViewController(self)
    }
    @IBAction func saveImageTapped(sender: UIButton) {
        UIImageWriteToSavedPhotosAlbum(editedImage!, nil,nil, nil)
        simpleAlert("Your picture has been saved to Photo Library!")

    }
    
    func showAlert(alertTitle: String, alertMessage: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func willMoveToParentViewController(parent: UIViewController?) {
        print("Test PArent")
        hideHUD()
    }
}
